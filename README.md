Ответы curl:

**********************************************************************************
```
curl -v -k -IL localhost:8080/home.html
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> HEAD /home.html HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 302 Moved Temporarily
HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.16.1
Server: nginx/1.16.1
< Date: Tue, 07 Jan 2020 07:44:43 GMT
Date: Tue, 07 Jan 2020 07:44:43 GMT
< Content-Type: text/html
Content-Type: text/html
< Content-Length: 145
Content-Length: 145
< Connection: keep-alive
Connection: keep-alive
< Location: https://localhost:8443/home.html
Location: https://localhost:8443/home.html

< 
* Connection #0 to host localhost left intact
* Issue another request to this URL: 'https://localhost:8443/home.html'
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: /etc/ssl/certs/ca-certificates.crt
  CApath: /etc/ssl/certs
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Client hello (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jan  7 07:35:34 2020 GMT
*  expire date: Jan  5 07:35:34 2025 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
> HEAD /home.html HTTP/1.1
> Host: localhost:8443
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
HTTP/1.1 200 OK
< Server: nginx/1.16.1
Server: nginx/1.16.1
< Date: Tue, 07 Jan 2020 07:44:43 GMT
Date: Tue, 07 Jan 2020 07:44:43 GMT
< Content-Type: text/html
Content-Type: text/html
< Content-Length: 558
Content-Length: 558
< Last-Modified: Tue, 07 Jan 2020 07:35:36 GMT
Last-Modified: Tue, 07 Jan 2020 07:35:36 GMT
< Connection: keep-alive
Connection: keep-alive
< ETag: "5e1434c8-22e"
ETag: "5e1434c8-22e"
< Accept-Ranges: bytes
Accept-Ranges: bytes

< 
* Connection #1 to host localhost left intact

```
***********************************************************************************
```
curl -v -k -IL localhost:8080

* Rebuilt URL to: localhost:8080/
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> HEAD / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 302 Moved Temporarily
HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.16.1
Server: nginx/1.16.1
< Date: Tue, 07 Jan 2020 07:43:32 GMT
Date: Tue, 07 Jan 2020 07:43:32 GMT
< Content-Type: text/html
Content-Type: text/html
< Content-Length: 145
Content-Length: 145
< Connection: keep-alive
Connection: keep-alive
< Location: https://localhost:8443/
Location: https://localhost:8443/

< 
* Connection #0 to host localhost left intact
* Issue another request to this URL: 'https://localhost:8443/'
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: /etc/ssl/certs/ca-certificates.crt
  CApath: /etc/ssl/certs
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Client hello (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jan  7 07:35:34 2020 GMT
*  expire date: Jan  5 07:35:34 2025 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
> HEAD / HTTP/1.1
> Host: localhost:8443
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 404 Not Found
HTTP/1.1 404 Not Found
< Server: nginx/1.16.1
Server: nginx/1.16.1
< Date: Tue, 07 Jan 2020 07:43:32 GMT
Date: Tue, 07 Jan 2020 07:43:32 GMT
< Content-Type: text/html
Content-Type: text/html
< Content-Length: 153
Content-Length: 153
< Connection: keep-alive
Connection: keep-alive

< 
* Connection #1 to host localhost left intact

```
***********************************************************************************
```
curl -v -k -IL localhost:8080/error
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> HEAD /error HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 302 Moved Temporarily
HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.16.1
Server: nginx/1.16.1
< Date: Tue, 07 Jan 2020 07:45:21 GMT
Date: Tue, 07 Jan 2020 07:45:21 GMT
< Content-Type: text/html
Content-Type: text/html
< Content-Length: 145
Content-Length: 145
< Connection: keep-alive
Connection: keep-alive
< Location: https://localhost:8443/error
Location: https://localhost:8443/error

< 
* Connection #0 to host localhost left intact
* Issue another request to this URL: 'https://localhost:8443/error'
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*   CAfile: /etc/ssl/certs/ca-certificates.crt
  CApath: /etc/ssl/certs
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Client hello (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jan  7 07:35:34 2020 GMT
*  expire date: Jan  5 07:35:34 2025 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
> HEAD /error HTTP/1.1
> Host: localhost:8443
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 404 Not Found
HTTP/1.1 404 Not Found
< Server: nginx/1.16.1
Server: nginx/1.16.1
< Date: Tue, 07 Jan 2020 07:45:21 GMT
Date: Tue, 07 Jan 2020 07:45:21 GMT
< Content-Type: text/html
Content-Type: text/html
< Content-Length: 153
Content-Length: 153
< Connection: keep-alive
Connection: keep-alive

< 
* Connection #1 to host localhost left intact
```

Ответы на вопросы:

1)Проблема в CORS, необходимо посмотреть какие хедеры и глаголы использует сервис и настроить на веб-сервере Access-Control-Request-Method, Access-Control-Request-Headers

2) Вероятнее всего закочнилось место на диске, поэтому и логи не пишутся, либо еще вариант закончились i-nodes, либо же лимиты на файловые дескрипторы в системе(Если конечно файл лога создается новый). Необходимо сделать мониторинг на свободное место и систему алерта в случае исчерпывания.

3) Позвать сисадмина, чтобы тот посмотрел логи. Вчера он должен был проверить все несколько раз и убедиться что все работает.

4) Вероятнее всего в это время система сильно нагружена. Настроить мониторинг, например заббикс, и смотреть что в это время нагружает систему. 